import { useState } from 'react';

const useCyptoCurrency = (label, initialState, cryptoList) => {
    // State de nuestro custom hook
    const [state, updateState] = useState(initialState);

    const SelectCrypto = () => {
        return (
            <>
                <label className="label">{label}</label>
                <select
                    className="select"
                    onChange={e => updateState(e.target.value)}
                    value={state}
                >
                    <option value="">-- Seleccione una moneda --</option>
                    {cryptoList.map((option) => {
                        return (
                            <option key={option.CoinInfo.id} value={option.CoinInfo.Name}>{option.CoinInfo.FullName}</option>
                        )
                    })}
                </select>
            </>
        )
    };

    // Retornar state, interfaz y función que modifica el state
    return [state, SelectCrypto, updateState];
}

export default useCyptoCurrency;