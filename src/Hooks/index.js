import useCurrency from './useCurrency/useCurrency';
import useCryptoCurrency from './useCryptoCurrency/useCryptoCurrency';

export {
    useCurrency,
    useCryptoCurrency
}