import { useState } from 'react';

const useCurrency = (label, initialState, MONEDAS) => {
    // State de nuestro custom hook
    const [state, updateState] = useState(initialState);

    const Select = () => {
        return (
            <>
                <label className="label">{label}</label>
                <select
                    className="select"
                    onChange={e => updateState(e.target.value)}
                    value={state}
                >
                    <option value="">-- Seleccione una moneda --</option>
                    {MONEDAS.map((option) => {
                        return (
                            <option key={option.code} value={option.code}>{option.name}</option>
                        )
                    })}
                </select>
            </>
        )
    };

    // Retornar state, interfaz y función que modifica el state
    return [state, Select, updateState];
}

export default useCurrency;

