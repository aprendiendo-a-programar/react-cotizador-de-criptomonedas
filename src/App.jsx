import { useState, useEffect } from 'react';
import { Form, CryptoValue, Spinner } from './Components/index';
import axios from 'axios';
import CryptoImg from './Img/cryptomonedas.png';
import './App.scss';

const App = () => {
  const [currency, setCurrency] = useState('');
  const [cryptoCurrency, setCryptoCurrency] = useState('');
  const [result, setResult] = useState({});
  const [download, setDownload] = useState(false);

  useEffect(() => {
    const cryptoPrice = async () => {
      // Evitar la ejecución la primera vez
      if (currency === '') return;

      // Consultar la API
      const url = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${cryptoCurrency}&tsyms=${currency}`;
      const resultAPI = await axios.get(url);

      // Mostrar spinner
      setDownload(true);

      // Ocultar spinner
      setTimeout(() => {
        // cambiar el estado de cargando
        setDownload(false);
        // guardar cotización
        setResult(resultAPI.data.DISPLAY[cryptoCurrency][currency]);
      }, 3000);
    }
    cryptoPrice();

  }, [currency, cryptoCurrency]);

  // Mostrar snipper o resultado
  const componentSpinner = (download) ? <Spinner /> : <CryptoValue result={result} />

  return (
    <div className="app">
      <div className="app__container1">
        <img src={CryptoImg} className="app__container1-img" alt="imagen de criptomonedas"></img>
      </div>
      <div className="app__container2">
        <h1 className="app__container2-title">Cotizador de criptomonedas</h1>
        <Form
          setCurrency={setCurrency}
          setCryptoCurrency={setCryptoCurrency}
        />

        {componentSpinner}
      </div>
    </div>
  );
}

export default App;
