import { useState, useEffect } from 'react';
import { useCurrency, useCryptoCurrency } from '../../Hooks/index';
import { Error } from '../index';
import axios from 'axios';
import './Form.scss';

const Form = ({setCurrency, setCryptoCurrency}) => {
    // State del listado de criptomonedas
    const [cryptoList, setCryptoList] = useState([]);
    const [error, setError] = useState(false);


    const MONEDAS = [
        { code: 'EUR', name: 'Euro' },
        { code: 'USD', name: 'Dolar de Estados Unidos' },
        { code: 'MXN', name: 'Peso Mexicano' },
        { code: 'GBP', name: 'Libra Esterlina' }
    ]

    // Utilizar useMoneda
    const [currency, SelectCurrency] = useCurrency('Elige tu moneda', '', MONEDAS);
    // Utilizar useCriptomonedas
    const [crypto, SelectCripto] = useCryptoCurrency('Elige tu criptomoneda', '', cryptoList);

    // Ejecutar llamado a la API
    useEffect(() => {
        const consultAPI = async () => {
            const url= 'https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD';
            const resultAPI = await axios.get(url);

            setCryptoList(resultAPI.data.Data);
        }

        consultAPI();
    }, []);

    // Cuando el usuario envia el formulario 
    const calculateCurrency = (e) => {
        e.preventDefault();

        // Revisar si ambos campos están llenos
        if(currency === '' || crypto === '') {
            setError(true);
            return;
        }

        // Pasar los datos al componente principal
        setError(false);
        setCurrency(currency);
        setCryptoCurrency(crypto);
    }

    return (
        <form className="form" onSubmit={calculateCurrency}>

            { error ? <Error message='Todos los campos son obligatorios'/> : null}

            <SelectCurrency />

            <SelectCripto />

            <button
                type="submit"
                className="form__button"
                value="Calcular"
            >Calcular</button>
        </form>
    );
}

export default Form;