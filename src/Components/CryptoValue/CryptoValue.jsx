import './CryptoValue.scss';

const CryptoValue = ({result}) => {
    if(Object.keys(result).length === 0) return null;

    return (
        <div className="crypto-value">
            <p className="crypto-value__text">El precio es: <span className="crypto-value__price">{result.PRICE}</span></p>
            <p className="crypto-value__text">Precio más alto del día: <span className="crypto-value__price">{result.HIGHDAY}</span></p>
            <p className="crypto-value__text">Precio más bajo del día: <span className="crypto-value__price">{result.LOWDAY}</span></p>
            <p className="crypto-value__text">Variación últimas 24h h: <span className="crypto-value__price">{result.CHANGEPCT24HOUR}</span></p>
            <p className="crypto-value__text">Última actualización: <span className="crypto-value__price">{result.LASTUPDATE}</span></p>
        </div>
    );
}
 
export default CryptoValue;